FROM alpine:3.12.0
RUN apk update && apk add --no-cache supervisor nginx python3 py-pip
WORKDIR /opt/challenge.hotels
ADD ./hotel_api .
ADD ./Instructions ./Instructions
RUN mv supervisor.conf /etc/supervisor.conf
RUN mkdir /var/log/carlypso /var/log/hotel_api
RUN pip install -r requirements.txt
ADD ./entrypoint.sh /entrypoint.sh
RUN chmod 755 /entrypoint.sh
EXPOSE 80 8000