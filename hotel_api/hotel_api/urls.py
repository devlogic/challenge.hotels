from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from rest_framework_nested import routers

from hotel_app import views
from hotel_app.views import HotelViewSet, FavoriteViewSet, ReviewViewSet, UserViewSet

router = routers.SimpleRouter()
router.register(r'hotels', HotelViewSet, base_name='hotels')
router.register(r'users', UserViewSet, base_name='users')

hotels_router = routers.NestedSimpleRouter(router, r'hotels', lookup='hotel')
hotels_router.register(r'reviews', ReviewViewSet, base_name='hotel-reviews')

users_router = routers.NestedSimpleRouter(router, r'users', lookup='user')
users_router.register(r'favorites', FavoriteViewSet, base_name='user-favorites')


urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^', include(hotels_router.urls)),
    url(r'^', include(users_router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api-token-auth/?', views.Login.as_view(), name='login'),
    url(r'^admin/', admin.site.urls),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
