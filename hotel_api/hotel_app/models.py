from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
from location_field.models.plain import PlainLocationField
from rest_framework.authtoken.models import Token


class Hotel(models.Model):
    name = models.CharField(max_length=200)
    city = models.CharField(max_length=200)
    country = models.CharField(max_length=100)
    image = models.ImageField(upload_to='images/', default='images/no-img.jpg')
    stars = models.IntegerField()
    description = models.TextField(max_length=2000)
    price = models.FloatField()
    date = models.DateTimeField(default=timezone.now)
    creator = models.ForeignKey(User, on_delete=models.CASCADE)
    location = PlainLocationField(based_fields=['city'], zoom=7)

    def __str__(self):
        return self.name + ', ' + self.city + ' - ' + self.country


class Review(models.Model):
    reviewer = models.ForeignKey(User, on_delete=models.CASCADE, related_name="reviews")
    message = models.TextField()
    created_at = models.DateTimeField(auto_now=True)
    is_positive = models.BooleanField()
    hotel = models.ForeignKey(Hotel, on_delete=models.CASCADE, related_name="reviews")

    def __str__(self):
        return self.reviewer.first_name + ' ' + self.reviewer.last_name + ' - ' + str(self.created_at.date())


class Favorite(models.Model):
    hotel = models.ForeignKey(Hotel, on_delete=models.CASCADE, related_name="favorites")
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="favorites")
    is_favorite = models.BooleanField()

    class Meta:
        unique_together = ("hotel", "user")


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)

