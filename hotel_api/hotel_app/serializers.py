from rest_framework import serializers
from .models import Hotel, Review, Favorite, User


class UserSerializer(serializers.ModelSerializer):
    def to_representation(self, instance):
        return {
            'id': instance.id,
            'username': instance.username,
            'first_name': instance.first_name,
            'last_name': instance.last_name,
            'email': instance.email,
            'is_staff': instance.is_staff
        }

    class Meta:
        model = User
        fields = ['username', 'password', 'email', 'first_name', 'last_name', 'is_staff']


class HotelSerializer(serializers.ModelSerializer):
    def to_representation(self, instance):
        return {
            'id': instance.id,
            'name': instance.name,
            'city': instance.city,
            'country': instance.country,
            'image': instance.image.url,
            'stars': instance.stars,
            'date': instance.date,
            'description': instance.description,
            'price': instance.price,
            'location': instance.location,
            'is_favorite': instance.is_favorite if hasattr(instance, 'is_favorite') else False,
        }

    class Meta:
        model = Hotel
        fields = ['id', 'name', 'city', 'country', 'image', 'stars', 'date',
                  'description', 'price', 'creator', 'location']


class ReviewSerializer(serializers.ModelSerializer):
    def to_representation(self, instance):
        return {
            'id': instance.id,
            'reviewer_first_name': instance.reviewer.first_name,
            'reviewer_last_name': instance.reviewer.last_name,
            'message': instance.message,
            'created_at': instance.created_at,
            'is_positive': instance.is_positive,
        }

    class Meta:
        model = Review
        fields = ['id', 'message', 'created_at', 'is_positive', 'reviewer', 'hotel']


class FavoriteSerializer(serializers.ModelSerializer):
    def to_representation(self, instance):
        return {
            'id': instance.id,
            'is_favorite': instance.is_favorite,
            'hotel_id': instance.hotel_id,
        }

    class Meta:
        model = Favorite
        fields = ['hotel', 'is_favorite', 'user']
