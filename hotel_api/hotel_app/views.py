from django.contrib.auth.hashers import make_password
from django.db.models import Subquery, OuterRef

from rest_framework import viewsets, status
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework import parsers, renderers
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Hotel, Favorite, Review
from .serializers import HotelSerializer, UserSerializer, ReviewSerializer, FavoriteSerializer


class Login(APIView):
    """
    API for login (obtaining token).
    """
    throttle_classes = ()
    permission_classes = ()
    parser_classes = (parsers.FormParser, parsers.MultiPartParser, parsers.JSONParser,)
    renderer_classes = (renderers.JSONRenderer,)
    serializer_class = AuthTokenSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)

        return Response({'token': token.key,
                         'username': user.username,
                         'first_name': user.first_name,
                         'last_name': user.last_name,
                         'user_id': user.id,
                         'email': user.email,
                         'is_staff': user.is_staff})


class UserViewSet(viewsets.ModelViewSet):
    permission_classes = (AllowAny,)
    serializer_class = UserSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.validated_data['password'] = make_password(serializer.validated_data['password'])
        self.perform_create(serializer)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def list(self, request, *args, **kwargs):
        return Response(status=status.HTTP_403_FORBIDDEN)

    def destroy(self, request, *args, **kwargs):
        return Response(status=status.HTTP_403_FORBIDDEN)

    def update(self, request, *args, **kwargs):
        return Response(status=status.HTTP_403_FORBIDDEN)

    def retrieve(self, request, *args, **kwargs):
        return Response(status=status.HTTP_403_FORBIDDEN)


class HotelViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = HotelSerializer

    def get_queryset(self):
        return Hotel.objects.all().annotate(
            is_favorite=Subquery(Favorite.objects.filter(
                hotel_id=OuterRef('pk'),
                user_id=self.request.user.id
            ).values('is_favorite')[:1])
        ).order_by('-date')


class ReviewViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = ReviewSerializer

    def get_queryset(self):
        hotel_id = self.kwargs['hotel_pk']
        if hotel_id:
            return Review.objects.filter(hotel=hotel_id)

        return Review.objects.none()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(
            data={
                'hotel': self.kwargs['hotel_pk'],
                'message': request.data['message'],
                'is_positive': request.data['is_positive'],
                'reviewer': request.data['reviewer']
            }
        )
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class FavoriteViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = FavoriteSerializer

    def get_queryset(self):
        is_favorite = self.request.query_params.get('is_favorite', None)
        if is_favorite:
            return Favorite.objects.filter(user=self.request.user.id, is_favorite=True).order_by('hotel__id')
        return Favorite.objects.filter(user=self.request.user.id).order_by('hotel__id')

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(
            data={
                'hotel': request.data['hotel'],
                'is_favorite': request.data['is_favorite'],
                'user': self.kwargs['user_pk']
            })
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        return Response(serializer.data, status=status.HTTP_201_CREATED)